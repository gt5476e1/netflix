$(document).ready(function(){
	$('#searchMovie').on('keyup', function(e) {
		let moviename = e.target.value;
		
		// Make request to Netflix
		$.ajax({
			url:'http://netflixroulette.net/api/api.php?title='+moviename,
			
		}).done(function(movie){
			$('#output').html(`

				
				<div class="panel panel-default">
				  <div class="panel-heading">				${movie.show_title}</div>
				  <div class="panel-body">
					<div class="row">
						<div class="col-md-3">
							<img class="thumbnail" style="width:100%;"src="${movie.poster}" />
						</div>
						<div class="col-md-9">
							<p>${movie.summary}</p>
							<a href="https://www.netflix.com/title/${movie.show_id}"><button class="btn btn-primary">Watch Now</button></a>
							<span class="label label-default">${movie.release_year}</span>
							<span class="label label-info">${movie.category}</span>
							<br><br>
							<p>Starrring: ${movie.show_cast}</p>
						</div>

				  </div>
				</div>
				
			`);
		});
	});

});